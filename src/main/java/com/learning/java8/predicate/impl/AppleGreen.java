package com.learning.java8.predicate.impl;

import com.learning.java8.dto.Apple;
import com.learning.java8.predicate.AppleFormater;

public class AppleGreen implements AppleFormater{

	public String show(Apple apple) {
		return "green".equals(apple.getColor())? "apple green" : "another color";
	}

}
