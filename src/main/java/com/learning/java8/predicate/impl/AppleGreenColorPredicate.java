package com.learning.java8.predicate.impl;

import com.learning.java8.dto.Apple;
import com.learning.java8.predicate.ApplePredicate;

public class AppleGreenColorPredicate implements ApplePredicate{

	public boolean test(Apple apple) {
		return "green".equals(apple.getColor());
	}

}
