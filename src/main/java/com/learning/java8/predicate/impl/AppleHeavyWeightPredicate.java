package com.learning.java8.predicate.impl;

import com.learning.java8.dto.Apple;
import com.learning.java8.predicate.ApplePredicate;

public class AppleHeavyWeightPredicate implements ApplePredicate{

	public boolean test(Apple apple) {
		return apple.getWeight() > 150;
	}

}
