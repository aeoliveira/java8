package com.learning.java8.predicate;

import com.learning.java8.dto.Apple;

public interface AppleFormater {
	String show(Apple apple);
}
