package com.learning.java8.lamba;

import java.util.List;
import java.util.function.Consumer;

import com.learning.java8.dto.Apple;
import com.learning.java8.predicate.ApplePredicate;

public class MainLamba {
	public void callPrint(List<Apple> list, Consumer<Apple> consumer) {
		for (Apple apple : list) {
			consumer.accept(apple);
		}
	}
	
	public void mainGreenColor(List<Apple> list, ApplePredicate applePredicate) {
		for (Apple apple : list) {
			if(applePredicate.test(apple)) {
				System.out.println("mainGreenColor...:" + apple.getColor());
			}
		}
	}
	
	
}
