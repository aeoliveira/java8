package com.learning.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;

import com.learning.java8.dto.Apple;
import com.learning.java8.lamba.MainLamba;
import com.learning.java8.predicate.ApplePredicate;

public class TestMethodReference {
	
	private List<Apple> getListTest(){
		List<Apple> listTest =  new ArrayList<Apple>();
		listTest.add(new Apple("green", 150));
		listTest.add(new Apple("blue", 10));
		listTest.add(new Apple("yellow", 180));
		listTest.add(new Apple("green", 250));
		listTest.add(new Apple("green", 50));

		return listTest;
	}
	
	@Test
	public void testPrintWithLamba() {
		Consumer<Apple> consumer = (Apple apple) -> System.out.println(apple.getColor());
		new MainLamba().callPrint(getListTest(), consumer);
	}
	
	@Test
	public void testApplePredicate() {
		new MainLamba().mainGreenColor(getListTest(), (Apple apple) -> "green".equals(apple.getColor()));
	}
}
