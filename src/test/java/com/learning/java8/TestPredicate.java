package com.learning.java8;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.learning.java8.dto.Apple;
import com.learning.java8.predicate.AppleFormater;
import com.learning.java8.predicate.ApplePredicate;
import com.learning.java8.predicate.impl.AppleGreen;
import com.learning.java8.predicate.impl.AppleGreenColorPredicate;
import com.learning.java8.predicate.impl.AppleHeavyWeightPredicate;

public class TestPredicate {
	
	private List<Apple> getListTest(){
		List<Apple> listTest =  new ArrayList<Apple>();
		listTest.add(new Apple("green", 150));
		listTest.add(new Apple("blue", 10));
		listTest.add(new Apple("yellow", 180));
		listTest.add(new Apple("green", 250));
		listTest.add(new Apple("green", 50));

		return listTest;
	}
	
	@Test
	public void testGreenColor() {
		List<Apple> result = new ArrayList<Apple>();
		
		ApplePredicate applePredicate = new AppleGreenColorPredicate();
		
		for (Apple apple : getListTest()) {
			if(applePredicate.test(apple)) {
				result.add(apple);
			}
		}
		
		assertEquals(3, result.size());
		
	}
	
	@Test
	public void testHeavyWeight() {
		List<Apple> result = new ArrayList<Apple>();
		
		ApplePredicate applePredicate = new AppleHeavyWeightPredicate();
		
		for (Apple apple : getListTest()) {
			if(applePredicate.test(apple)) {
				result.add(apple);
			}
		}
		
		assertEquals(2, result.size());
		
	}
	
	@Test
	public void testShowGreenColor() {
		AppleFormater appleFormater = new AppleGreen();
		
		for (Apple apple : getListTest()) {
			System.out.println(appleFormater.show(apple));
		}
		
		assertTrue(true);
		
	}
	
	
}
